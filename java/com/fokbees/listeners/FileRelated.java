package com.fokbees.listeners;

import java.awt.Color;

import de.btobastian.javacord.DiscordAPI;
import de.btobastian.javacord.entities.message.Message;
import de.btobastian.javacord.entities.message.embed.EmbedBuilder;
import de.btobastian.javacord.listener.message.MessageCreateListener;

public class FileRelated implements MessageCreateListener {

	public void onMessageCreate(DiscordAPI api, Message message) {
		String msg = message.getContent().toLowerCase();

		if (msg.equalsIgnoreCase("!avatar")) {
			EmbedBuilder cora = new EmbedBuilder();
			String AvatarURL = message.getAuthor().getAvatarUrl().toString();
			cora.setImage(AvatarURL);
			cora.setTitle(message.getAuthor().getName() + "'s Avatar.");
			cora.setUrl(AvatarURL);
			cora.setColor(Color.cyan);
			message.reply(null, cora);

		}

		if (msg.equalsIgnoreCase("!groupicon")) {
			EmbedBuilder cora_group = new EmbedBuilder();
			String GroupURL = message.getChannelReceiver().getServer().getIconUrl().toString();
			cora_group.setImage(GroupURL);
			cora_group.setTitle(message.getChannelReceiver().getServer().getName() + " info");
			cora_group.setUrl(GroupURL);
			cora_group.setColor(Color.cyan);
			message.reply(null, cora_group);
		}

	}

}
