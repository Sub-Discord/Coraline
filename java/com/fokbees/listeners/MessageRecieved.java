package com.fokbees.listeners;

import java.awt.Color;
import java.util.TimeZone;

import de.btobastian.javacord.DiscordAPI;
import de.btobastian.javacord.entities.Server;
import de.btobastian.javacord.entities.message.Message;
import de.btobastian.javacord.entities.message.embed.EmbedBuilder;
import de.btobastian.javacord.listener.message.MessageCreateListener;

public class MessageRecieved implements MessageCreateListener {

	public void onMessageCreate(DiscordAPI api, Message message) {
		String msg = message.getContent().toLowerCase();

		if (msg.equalsIgnoreCase("!help")) {
			message.getAuthor().sendMessage("piss off, im incapable");
		}

		if (msg.equalsIgnoreCase("!test")) {
			EmbedBuilder ServerInfo = new EmbedBuilder();
			Server Server = message.getChannelReceiver().getServer();
			String GroupURL = Server.getIconUrl().toString();
			ServerInfo.setTitle(Server.getName() + " info");
			ServerInfo.setDescription("" + Server.getCreationDate().toInstant());
			ServerInfo.setColor(Color.cyan);
			ServerInfo.addField("Users:", "" + Server.getMemberCount(), true);
			ServerInfo.addField("Server name:", Server.getName(), false);
			ServerInfo.addField("Server ID:", "" + Server.getId(), false);
			ServerInfo.addField("Region:", "" + Server.getRegion().getName(), false);
			ServerInfo.addField("Owner ID:", "" + Server.getOwnerId(), false);
			ServerInfo.setThumbnail(GroupURL);
			message.reply(null, ServerInfo);
		}

	}

}
